#: -*- coding: utf-8 -*-
import os
if os.name == 'Windows':
	from twisted.internet import iocpreactor
	iocpreactor.install()
from twisted.internet import reactor
from twisted.application import service
from twisted.python import log
import logging
from txsockjs.factory import SockJSFactory
from protocols import WebSocketProtocol, WebSocketFactory, MessengerClientFactory
import errno
from logging.handlers import TimedRotatingFileHandler

VERSION = "1.0.0"

DEBUG = True

LOG_LEVEL = logging.WARNING

#: хост сервера webSocket
LISTEN_HOST = '127.0.0.1'
#: номер порта сервера webSocket
LISTEN_PORT = 9090
#: хост Messenger
MESSENGER_HOST = '127.0.0.1'
#: номер порта Messenger для администрирования
MESSENGER_PORT = 7559

#: название приложения
APP_NAME = 'MolniaTerminal'
#: путь до приложения
APP_DIR = os.path.abspath(os.path.dirname(__file__))
#: форматирование отображения времени
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
#: форматирование отображения времени
SHORT_DATETIME_FORMAT = "%H:%M:%S"

#####################
#: НАСТРОЙКА ЛОГОВ :#
#####################
#: путь до файла кофигарции
CONFIG_DIR = os.path.abspath(os.path.dirname(__file__))
#: путь до места хранения логов
LOGS_DIR = os.path.join(APP_DIR, 'logs')
#: название файла лога
LOG_FILENAME = os.path.join(LOGS_DIR, APP_NAME + '.log')
#: количество дней за которые будет храниться лог
LOGS_DAYS_KEEP = 7

def configure_logs():
	""" Настраивает журналирование. В зависимости от режима работы выбирает
	способ журналирования.Создает папку для хранения логов если необходимо
	"""
	if DEBUG:
		logging.basicConfig(level=logging.DEBUG)
	else:
		#: прежде всего создаем папку для хранения, если ее нет
		try:
			os.makedirs(LOGS_DIR)
		except OSError as exception:
			if exception.errno != errno.EEXIST:
				raise
		#: делим логи по дням
		handler = TimedRotatingFileHandler(LOG_FILENAME,
		                                   when="midnight",
		                                   backupCount=LOGS_DAYS_KEEP)
		format = '%(asctime)s: %(levelname)-6s %(message)s'
		log_format=logging.Formatter(fmt=format, datefmt='%y-%m-%d %H:%M:%S')
		handler.setFormatter(log_format)

		logger = logging.getLogger(APP_NAME)
		logger.setLevel(LOG_LEVEL)
		logger.addHandler(handler)
		#: start message
		logger.info('='*79)

class MolniaAgent(service.Service):
	"""
	Класс контейнер связывающий между собой все части приложения
	"""
	def __init__(self):
		self.sockjs_factory = None
		#: настраиваем журналы на вывод через стандартный библиотечный модуль
		observer = log.PythonLoggingObserver(loggerName=APP_NAME)
		observer.start()

	def writeProtocol(self, p):
		self.messenger_factory.mprotocol.write('stats')

	def setup(self):
		""" Создает модули(части) системы и осуществляет их взаимосвязь
		"""
		#: Создаем сервер WebSocket SockJS
		self.webSocket_factory = WebSocketFactory.forProtocol(WebSocketProtocol)
		self.sockjs_factory = SockJSFactory(self.webSocket_factory)
		reactor.listenTCP(LISTEN_PORT, self.sockjs_factory, interface=LISTEN_HOST)
		#: Создаем клиента Messenger
		self.messenger_factory = MessengerClientFactory();
		reactor.connectTCP(MESSENGER_HOST, MESSENGER_PORT, self.messenger_factory)
		#self.webSocket_factory.read = self.webSocket_factory.shout
		self.webSocket_factory.MolniaAgent = self
		self.messenger_factory.MolniaAgent = self

if __name__ == '__main__':
	#: подготавливаем систему
	configure_logs()
	#: настраиваем и запускаем приложение
	app = MolniaAgent()
	app.setup()
	reactor.run()