from django.shortcuts import render
from MIPClient import settings

def index(request):
	return render(request, "index.html")