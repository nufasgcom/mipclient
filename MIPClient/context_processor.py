from MIPClient import settings


def global_vars(context):
	return {
		'BASE_URL': '{}'.format(settings.BASE_URL),
		'STATIC_URL': '{}{}'.format(settings.BASE_URL, settings.STATIC_URL),
		'MEDIA_URL': '{}{}'.format(settings.BASE_URL, settings.MEDIA_URL),
		'WEB_SOCKET_URL': '{}'.format(settings.WEB_SOCKET_URL)
	}