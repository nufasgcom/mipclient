#: -*- coding: utf-8 -*-
import json

from twisted.internet.protocol import Protocol, Factory, ClientFactory
from twisted.python import log
import time


class WebSocketProtocol(Protocol):

	def dataReceived(self, data):
		"""Парсим сообщение и определяем есть ли в нем канал для подписки
		"""

		log.msg('Recived data "{data}"'.format(data=data))
		self.factory.subscribe(self)
		host = self.transport.transport.request.client.host
		port = self.transport.transport.request.client.port
		data = json.loads(data)
		cmd = data.get('cmd' or None)
		if cmd:
			cmd = cmd.encode('utf-8')
		msg = data.get('msg' or None)
		if msg:
			msg = msg.encode('utf-8')
		user = data.get('user' or 'HACK_USER')
		if user:
			user = user.encode('utf-8')
		if cmd: #: Если пришла команда
			if self.factory.MolniaAgent != None:
				MAgent = self.factory.MolniaAgent
				hasprotokol = MAgent.messenger_factory!=None and MAgent.messenger_factory.protocol != None and MAgent.messenger_factory.protocol.connected
				if hasprotokol:
					log.msg('toMessenger-> {txt}'.format(txt=cmd))
					MAgent.messenger_factory.protocol.write(cmd)
					self.factory.shout('{host}:{user} : {txt}'.format(host=host, user=user, txt=cmd))
				else:
					log.err('Not connect to Messenger (protocol)')
					self.factory.shout('{host}:{user} : {txt}'.format(host=host, user=user, txt=cmd))
					cmd = 'No connect to Messenger (protocol). Please wait several seconds, аnd repeat.'
					self.factory.shout('{host}:{user} : {txt}'.format(host=host, user=user, txt=cmd))
			else:
				log.msg('Not connect to Messenger (factory)')
				self.factory.shout('{host}:{user} : {txt}'.format(host=host, user=user, txt=cmd))
				cmd = 'No connect to Messenger (factory). Please wait several seconds, аnd repeat.'
				self.factory.shout('{host}:{user} : {txt}'.format(host=host, user=user, txt=cmd))
		elif msg: #: Если пришло сообщение
			self.factory.shout('{host}:{user} : Message - "{txt}"'.format(host=host, user=user, txt=msg))


	def notify(self, message):
		self.transport.write(message)

	def connectionLost(self, reason):
		"""Удаляем ссылку на соединение из фабрики
		"""
		self.factory.unsubscribe(self)


class WebSocketFactory(Factory):
	def __init__(self):
		self.connections = []
		self.MolniaAgent = None

	def subscribe(self, conn):
		"""Сохраняет соединение в массиве подписанных на канал
		"""
		#: if reconnect to other channel
		#self.unsubscribe(conn)
		if not conn in self.connections:
			self.connections.append(conn)
			host = conn.transport.transport.request.client.host
			self.shout('{host}: connected'.format(host=host))

	def unsubscribe(self, conn):
		"""Удаляет соединение из подписок
		"""
		if conn in self.connections:
			self.connections.remove(conn)


	def shout(self, data):
		"""Оповещает все соединения
		"""
		log.msg('shout')
		# data = json.loads(data)
		log.msg(data)
		'''if 'message' in data.keys():
			channels, message = data['channel'], json.dumps(data['message'])
			for ch in self.connections:
				for conn in self.connections[ch]:
					conn.notify(message)
		'''
		for conn in self.connections:
			conn.notify(data)

class MessengerProtocol(Protocol):
	def __init__(self):
		factory = None

	def connectionLost(self, reason):
		self.connected = False

	def dataReceived(self, data):
		log.msg('from Messenger->{txt}'.format(txt=data))
		if self.factory.MolniaAgent == None:
			log.error('Nо MolniaAgent in MessengerProtocol')
		else:
			log.msg('Received from messenger {txt}'.format(txt=data))
			MAgent = self.factory.MolniaAgent
			MAgent.webSocket_factory.shout('Messenger : {txt}'.format(txt=data))


	def write(self, data):
		log.msg('to Messenger->{txt}'.format(txt=data))
		data = '{txt}\r\n'.format(txt=data).encode('utf-8')
		self.transport.write(data)

class MessengerClientFactory(ClientFactory):
	def __init__(self):
		self.MolniaAgent = None

	def startedConnecting(self, connector):
		log.msg('Connect to Messenger.')

	def buildProtocol(self, addr):
		log.msg('Connected to Messenger.')
		self.protocol = MessengerProtocol()
		self.protocol.factory = self
		return self.protocol

	def clientConnectionLost(self, connector, reason):
		log.err('Lost connection to Messenger.  Reason:', reason)
		self.reconnect(connector)

	def clientConnectionFailed(self, connector, reason):
		log.err('Connection failed to Messenger. Reason:', reason)
		self.reconnect(connector)

	def reconnect(self, connector, sleep_sec=5):
		log.msg('After {sleep_sec} seconds will attempt to Messeger reconnect'.format(sleep_sec=sleep_sec))
		time.sleep(sleep_sec)
		log.msg('Reconnect to Messeger.'.format(sleep_sec=sleep_sec))
		connector.connect()
