from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
class Terminal(models.Model):
	id = models.AutoField(primary_key=True, help_text="Unique ID for terminal journal")
	date = models.DateField(auto_now_add=True)
	user = models.ForeignKey(User, default=None)
	terminal = models.TextField(null=False, blank=False, default='')
	class Meta:
		permissions = (("can_use_terminal", "Can use terminal"),)