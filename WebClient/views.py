from django.shortcuts import render
from MIPClient import settings
from django.contrib.auth.decorators import permission_required
from WebClient.models import Terminal
import logging

#import client.py

logger = logging.getLogger(__name__)

def my_permission_required(perm):
	return permission_required(perm, settings.LOGIN_REDIRECT_URL)

@my_permission_required('WebClient.can_use_terminal')
def terminal(request, id=None):
	if id == None:
		device = ''
	else:
		device = str(id)
	terminal = Terminal(user_id=request.user.id)
	terminal.save()
	request.session['term_log_id'] = terminal.id
	logger.debug('Create record in Terminal with id="{}"'.format(id))
	return render(request, "terminal.html", locals() )#{"device": device, "URL": wsurl, "static": settings.STATIC_URL, })

@my_permission_required('WebClient.can_use_terminal')
def log(request):
	try:
		rec = None
		ret = 'No valid'
		id = request.session.get('term_log_id', None)
		if id is not None:
			rec = Terminal.objects.get(id=id)
			if rec is None:
				logger.warn('Not get record in Terminal by ID. (Why?). Create new record.')
				rec = Terminal(user_id=request.user.id)
				rec.save()
				request.session['term_log_id'] = rec.id
		else:
			logger.warn('ID is None. (Why?). Create new record.')
			rec = Terminal(user_id=request.user.id)
			rec.save()
			request.session['term_log_id'] = rec.id
		rec.terminal += request.POST['text']+'\r\n'
		rec.save()
		ret = 'Success'
	except Exception:
		logger.error('Error save terminal log')
		ret = 'Fail'
	finally:
		return render(request, "ajax_term.html", locals())

