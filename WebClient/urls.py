from django.conf.urls import url
from WebClient import views

urlpatterns = [
    url(r'^$', views.terminal, name = 'terminal'),
    url(r'^log$', views.log, name = 'log'),
]
