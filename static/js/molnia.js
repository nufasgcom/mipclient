    EMPTY= 'Empty'
    webSocket = {}

    window.onload = function() { // Начальные действия
        forHide = document.getElementById('loading');
        forHide.hidden = true;
        forShow = document.getElementById('interface');
        forShow.hidden = false;
        ConnectToWebSocket(connect.getAttribute('url'))
    }

    function ConnectToWebSocket(url){
        printToTerminal('Attempt connect to WebSocket by url:'+WEB_SOCKET_URL)
        webSocket = createSocket(WEB_SOCKET_URL);// определение переменной в terminal.html
    }

    function terminal_clear_click(elem){ // Обработчик очистки терминала
        terminal.value='';
    }

    function add_cmd_click(elem){ // Обработчик добавления командной строки
        divCmds = document.querySelectorAll('div.cmd_div');
        divCmd = divCmds[divCmds.length - 1]
        div = document.createElement('div');
        div.className = 'cmd_div';
        div.innerHTML = divCmd.innerHTML
        divCmd.after(div)
        elements = document.body.querySelectorAll('button.del_cmd_btn');
        for (el of elements) {
            el.hidden = false;
        }
    }

    function del_cmd_click(elem){ // Обработчик удаления командной строки
        divCmds = document.querySelectorAll('div.cmd_div');
        if (divCmds.length > 1) { // Смотрим что бы оставалась одна командная строка
            divCmd = elem.parentElement;
            divCmd.remove();
            if (divCmds.length == 2) {
                el = document.body.querySelector('button.del_cmd_btn');
                el.hidden = true;
            }
        }
    }

    function setEmptyErrClass(elem) { // если значение элемента равно "" добавить класс EMPTY
        if  (!!elem) {
            if (elem.value.trim() == '') {
                elem.classList.add(EMPTY)
            } else {
                elem.classList.remove(EMPTY)
            }
        }
    }

    function getSendButton(elem) { // Получаем элемент кнопки отправки комманд по соседнему элементу
        parent = elem.parentElement;
        el = parent.querySelector('button.send_cmd_btn');
        return el;
    }

    function getCmd(elem) { // Получаем элемент ввода команд по соседнему элементу
        parent = elem.parentElement;
        el = parent.querySelector('input.cmd');
        setEmptyErrClass(el)
        return el;
    }

    function CreateRequest()
    {
        var Request = false;

        if (window.XMLHttpRequest)
        {
            //Gecko-совместимые браузеры, Safari, Konqueror
            Request = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            //Internet explorer
            try
            {
                 Request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (CatchException)
            {
                 Request = new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        if (!Request)
        {
            console.log("Невозможно создать XMLHttpRequest");
        }
        return Request;
    }

    /*
Функция посылки запроса к файлу на сервере
r_method  - тип запроса: GET или POST
r_path    - путь к файлу
r_args    - аргументы вида a=1&b=2&c=3...
r_handler - функция-обработчик ответа от сервера
*/
function SendRequest(r_method, r_path, r_args, r_handler)
{
    //Создаём запрос
    var Request = CreateRequest();

    //Проверяем существование запроса еще раз
    if (!Request)
    {
        return;
    }

    //Назначаем пользовательский обработчик
    Request.onreadystatechange = function()
    {
        //Если обмен данными завершен
        if (Request.readyState == 4)
        {
            //Передаем управление обработчику пользователя
            r_handler(Request);
        }
    }

    //Проверяем, если требуется сделать GET-запрос
    if (r_method.toLowerCase() == "get" && r_args.length > 0)
    r_path += "?" + r_args;

    //Инициализируем соединение
    Request.open(r_method, r_path, true);

    if (r_method.toLowerCase() == "post")
    {
        //Если это POST-запрос

        //Устанавливаем заголовок
        Request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
        //Посылаем запрос
        Request.send(r_args);
    }
    else
    {
        //Если это GET-запрос

        //Посылаем нуль-запрос
        Request.send(null);
    }
}

    // возвращает куки с указанным name,
    // или undefined, если ничего не найдено
    function getCookie(name) {
      let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function SendToDB(text){ // Ассинхронная отправка сообщения на сервер для записи в БД
        var Handler = function (Request) {
            console.log(Request.responseText)
        }
        csrf_token = getCookie('csrftoken')
        arg = 'text='+text+'&csrfmiddlewaretoken='+csrf_token
        SendRequest("POST", URL+'/terminal/log', arg, Handler); // URL описан terminal.html
    }

    function printToTerminal(text) {  // Вывод в терминал
        terminal.value = terminal.value + text.trim() +  '\r\n';
        SendToDB(text.trim())
        //alert(terminal.scrollTop);
        terminal.scrollTop = terminal.scrollHeight - terminal.clientHeight;
    }

    function send_cmd_click(e, elem){ // Обработчик нажатия кнопки посылки команды
        //alert(e.type);
        e.preventDefault();
        e.stopPropagation()
        cmd = getCmd(elem)
        sendCmd(cmd.value);
    }

    function send_msg_click(e, elem){ // Обработчик нажатия кнопки посылки сообщения
        e.preventDefault();
        e.stopPropagation()
        sendMsg(msg.value);
        msg.value = '';
    }

    function onBlurCMD() {
        elements = document.querySelectorAll('input.cmd');
        val = ''
        alert(elements.length)
        for (elem in elements) {
            if (elem) {
                val +='{|}' + elem.value;
            }
        }
        setCookie ('cmds',val);
        alert(getCookie('cmds'));
    }

    function setCookie(name, value, options = {}) {

      options = {
        path: '/',
        // при необходимости добавьте другие значения по умолчанию
        ...options
      };

      if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
      }

      let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

      for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
          updatedCookie += "=" + optionValue;
        }
      }

      document.cookie = updatedCookie;
    }

    function onPressCmd(elem) { // Обработчик нажатия кнопки посылки сообщения
        if (event.key != 'Enter'){
            setEmptyErrClass(elem)
            //device_id = getDevice(elem);
            cmd = getCmd(elem);
            sendButton = getSendButton(elem);
            if (cmd.classList.contains(EMPTY)) {
                 sendButton.setAttribute('disabled','disabled')
            } else {
                 sendButton.removeAttribute('disabled')
            }
        } else {
            if (event.type == 'keypress') {
                send_cmd_click(event,elem);
            }
        }
    }

    function disableSendButtons(){ // Деактивация кнопок отправки
        cmdButtons = document.querySelectorAll('button.send_cmd_btn'); // Кнопки отправки комманд
        setsAttribute(cmdButtons, 'disabled','disabled');
        msgButtons = document.querySelectorAll('button.send_msg_btn');// Кнопка(и) отправки сообщений
        setsAttribute(msgButtons, 'disabled','disabled');
    }

    function enableSendButtons(){// Активация кнопок отправки
        msgButtons = document.querySelectorAll('button.send_msg_btn');// Кнопка(и) отправки сообщений
        removesAttribute(msgButtons, 'disabled');
        // Кнопки отправки комманд активировать если заполнены соответствующие поля комманд
        cmdButtons = document.querySelectorAll('button.send_cmd_btn'); // Кнопки отправки комманд
        for (btn of cmdButtons) {
            cmd=getCmd(btn)
            val = cmd.value
            val = val.trim()
            if (val != '') {
                btn.removeAttribute('disabled');
            }
        }
    }

    function createSocket(url){ // Создание WebSocket
        var sock = new SockJS(url);
         sock.onopen = function() {
             console.log('open');
             sendMsg('User "'+USERNAME+'" connected to Terminal'); // + this.url
             enableSendButtons();
             connect.setAttribute('disabled','disabled');
         };

         sock.onmessage = function(e) {
             //console.log('message', e.data);
             printToTerminal(e.data)
         };

         sock.onclose = function() {
             console.log('close');
             printToTerminal('WARNING Connection closed');
             disableSendButtons();
             connect.removeAttribute('disabled');
         };
         return sock;
    };

    function setsAttribute(elements, attr_name, attr_value){
        for (elem of elements) {
            elem.setAttribute(attr_name, attr_value);
        }
    }

    function removesAttribute(elements, attr_name){
        for (elem of elements) {
            elem.removeAttribute(attr_name);
        }
    }

    function sendCmd(cmd){ // Функция отправки команды
        message = {};
        message.cmd = cmd;
        message.user = USERNAME;
        jsonMessage = JSON.stringify(message);
        //printToTerminal(jsonMessage);
        webSocket.send(jsonMessage);
    }

    function sendMsg(txt){ // Функция отправки сообщения (если подключится несколько пользователей то смогут переписываьтся)
        message = {};
        message.msg = txt;
        message.user = USERNAME;
        jsonMessage = JSON.stringify(message);
        //printToTerminal(jsonMessage);
        webSocket.send(jsonMessage);
    }

    function changeLineBreak(){ // Включить/выключить перенос
        //lineBreak.checked = !lineBreak.checked;
        if (lineBreak.checked) {
            terminal.setAttribute('wrap','soft')
        } else {
            terminal.setAttribute('wrap','off')
        }
    }